---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Talgreining fyrir Samróm

Þetta er talgreiningarkerfi fyrir málheild Samróms sem notar [Kaldi](http://kaldi-asr.org/).  
Góðar leiðbeiningar á íslensku eru til staðar á [GitLab hirslu kerfisins](https://gitlab.com/icelandic-lt/mt/SMT#moses-pbsmt)

### Heimildir:
[GitLab. Samromur-asr.](https://gitlab.com/icelandic-lt/asr/samromur-asr)