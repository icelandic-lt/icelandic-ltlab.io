---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Greynir

Greynir er Python pakki til þess að vinna með íslenskan texta.  
Hugbúnaðarpakkinn getur þáttað texta í setningatré, fundið uppflettimyndir, beygt nafnliði, markað texta og margt fleira.  
Greynir notar [Tokenizer](/howto/nlp/tokenizer) tilreiðarann til að tilreiða texta.  

Vinnsla texta og málfræðigreining með Greyni getur verið mikilvæg undirstaða til að keyra önnur tól eða forrit, t.d. fyrir fyrirspurnakerfi, upplýsingaheimt, leitartól, til að finna og greina mynstur eða líkindi í texta, til að greina höfundareinkenni, til að útbúa samantekt, flokka efni o.s.frv.[^1]  

Ítarlegri skjölun um Greyni (á ensku) má finna [hér](https://greynir.is/doc/).  

### Tenglar:
[^1]: [Greynir. "Enabling your application"](https://greynir.is/doc/index.html#enabling-your-application)
[GitLab. GreynirPackage.](https://gitlab.com/icelandic-lt/nlp/GreynirPackage)  
