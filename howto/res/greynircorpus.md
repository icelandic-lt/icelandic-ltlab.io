---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# GreynirCorpus

[GreynirCorpus](https://gitlab.com/icelandic-lt/resources/GreynirCorpus) er stórt safn af fullþáttuðum texta á nútímaíslensku gefinn út af [Miðeind ehf.](https://mideind.is)  

Nánari upplýsingar á [GitLab síðu málheildarinnar](https://gitlab.com/icelandic-lt/resources/GreynirCorpus#st%C3%B3r-trj%C3%A1banki-me%C3%B0-%C3%BE%C3%A1ttu%C3%B0um-%C3%ADslenskum-texta).  

### Heimildir:
[GitLab. GreynirCorpus.](https://gitlab.com/icelandic-lt/resources/GreynirCorpus)