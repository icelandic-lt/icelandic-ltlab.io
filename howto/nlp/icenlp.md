---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# IceNLP

IceNLP er opið NLP (Natural Language Processing) tólasett fyrir greiningu og vinnslu íslensks texta.
IceNLP er í tveimur hlutum:
- Kjarna (core) - inniheldur kjarnaverkfæri eins og tilreiðara, málfræðilega markara, stöðuþáttara (e. finite state parser), lemmald og nafnakennslatól (e. Named Entity Recogniser).  
- Þjón (server)

Þáttarinn í IceNLP er hlutaþáttari, þ.e. hann þáttar ekki setningar í setningatré eins og GreynirPackage heldur dregur saman setningarliði innan hverrar setningar. Sjá samanburð á [yfirlitssíðunni](/howto/prufa).

### Uppsetning:

Á Ubuntu/Debian þarf nokkra pakka áður en kjarni er samþýddur (e. compiled):  
`sudo apt-get install -y jflex ant openjdk-7-jdk texlive-latex-extra texlive-fonts-recommended`  
Svo má samþýða með eftirfarandi skipun (ath. þjóninn má samþýða með sömu skipun):  
`ant`  


### Notkun:

Hægt er að keyra upp þjóninn  

`/server/sh/RunServer.sh`  

og eiga samskipti við hann með:  

`echo "Hann er góður kennari" | ./RunClient.sh`  


### Tenglar:
[GitLab. IceNLP.](https://gitlab.com/icelandic-lt/nlp/icenlp)