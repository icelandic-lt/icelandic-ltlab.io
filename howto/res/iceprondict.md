---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Íslensk framburðarorðabók fyrir máltækni

Taltæknihugbúnaður, talgreining og talgerving, nýtir sérstakar framburðarorðabækur þar sem orð eru umrituð með sérstöku hljóðritunarstafrófi sem lýsir framburði orða nákvæmar en hefðbundin stafsetning.

Íslensk framburðarorðabók inniheldur handyfirfarnar hljóðritanir í fjórum framburðartilbrigðum íslensku:  
það sem kalla má hefðbundinn framburð, norðlenskt harðmæli, harðmæli + raddaðan framburð sem einkennandi er fyrir norð-austurland, og sunnlenskan hv-framburð. 

Hirslan inniheldur einnig þjálfunar- og prófunargögn fyrir þjálfun grapheme-to-phoneme (g2p) líkana, sem og prófunarsett fyrir sjálfvirka atkvæðaskiptingu og áherslumerkingar. Þessi líkön eru notuð til þess að hljóðrita ný orð sjálfvirkt.

### Dæmi:
![iceprondict](/assets/img/iceprondict-img1.png)  


### Heimildir:
[GitLab. Iceprondict.](https://gitlab.com/icelandic-lt/resources/iceprondict#%C3%ADslensk-frambur%C3%B0aror%C3%B0ab%C3%B3k-fyrir-m%C3%A1lt%C3%A6kni)