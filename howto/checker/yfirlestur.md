---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Yfirlestur.is

[Yfirlestur.is](https://yfirlestur.is/) er vefsíða fyrir almenning þar sem hægt er að skrifa eða senda inn íslenskan texta og finna stafsetningar- og málfarsvillur.  
![Mynd af Yfirlestur.is](/assets/img/yfirlestur-img1.png)  


### Notkun og dæmi:

Helsta leiðin til að nota vefsíðuna er einfaldlega með vafra á [Yfirlestur.is](https://yfirlestur.is/), til að lesa yfir texta og fá leiðréttingar og tillögur.  

En einnig er hægt að nota forritaskil (e. API) vefsíðunnar beint af skipanalínu (með `curl`) og fá JSON til baka:  

`curl https://yfirlestur.is/correct.api -d "text=Bíll"`  

![dæmi 1](/assets/img/yfirlestur-example1.png)  


Python forritaskil (e. API) eru á [GitLab síðu Yfirlesturs](https://gitlab.com/icelandic-lt/checker/Yfirlestur#from-python)).  

Tókakóðar eru úr tilreiðaranum [Tokenizer](/howto/nlp/tokenizer).  

#### Tenglar:
[GitLab. Yfirlestur.](https://gitlab.com/icelandic-lt/checker/Yfirlestur)