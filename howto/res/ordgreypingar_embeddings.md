---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Greypingar / Embeddings

Þessi hirsla hýsir gagnasöfn sem meta getu orðvigralíkana til þess að átta sig á merkingarfræði og orðhlutafræði íslensku, auk forþjálfaðra greypinga sem hafa verið metnar með téðum gagnasöfnum.  
Nánari upplýsingar og leiðbeiningar má finna á [GitLab hirslunni](https://gitlab.com/icelandic-lt/resources/ordgreypingar_embeddings).

## Hvað er orðagreyping?

Orðagreyping (e. [word embedding](https://en.wikipedia.org/wiki/Word_embedding)) er framsetning orða sem vigurs við greiningu texta.  

### Heimildir:
[Ordgreypingar_embeddings, GitLab](https://gitlab.com/icelandic-lt/resources/ordgreypingar_embeddings/-/blob/main/LESTU.md)