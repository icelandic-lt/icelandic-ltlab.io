---
title: SÍM 
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

## Stofnun Árna Magnússonar í íslenskum fræðum
[https://arnastofnun.is/is](https://arnastofnun.is/is)

Tengiliður: Steinþór Steingrímsson, <steinthor.steingrimsson@arnastofnun.is>

Árnastofnun er miðstöð __málfanga fyrir íslensku__, þar er unnið að margvíslegum málheildum og þróun orðfræðigagna fyrir íslensku og íslenska máltækni. Þróun málfanga er helsta hlutverk Árnastofnunar innan máltækniáætlunar, en einnig kemur stofnunin að þróun stoðtóla og veitir margvíslega ráðgjöf, t.d. í leyfismálum.

## Háskólinn í Reykjavík
[https://lvl.ru.is/](https://lvl.ru.is/)

Tengiliðir: Jón Guðnason, <jg@ru.is> og Hrafn Loftsson, <hrafn@ru.is>

Háskólinn í Reykjavík hýsir Mál- og raddtæknistofu þar sem stundaðar eru rannsóknir á tali og máltækni. Aðkomu Mál- og raddtæknistofu að máltækniáætluninni má skipta í tvennt: annars vegar rannsóknum og þróun í __taltækni__ og hins vegar í rannsóknum og þróun sem snúa að textum, fyrst og fremst __stoðtólum og vélþýðingum__. HR stýrir söfnunum á talgögnum fyrir talgreiningu og talgervingu fyrir íslensku, m.a. í gegnum Samróm. Háskólinn í Reykjavík býður í samvinnu við Háskóla Íslands upp á meistara- og doktorsnám í máltækni.

## Háskóli Íslands
[http://linguist.is/language-and-technology-lab/](http://linguist.is/language-and-technology-lab/)

Tengiliður: Anton Karl Ingason, <antoni@hi.is>

Innan HÍ er unnið að fjölbreyttum rannsóknum í máltækni. Aðkoma HÍ að máltækniáætlun snýr fyrst og fremst að __málrýni__, söfnun og greiningu gagna um mál- og ritvillur í textum og hugbúnaðarþróun. Háskóli Íslands býður í samvinnu við Háskólann í Reykjavík upp á meistara- og doktorsnám í máltækni.
Háskóli Íslands sinnir verkefnisstjórn frá hausti 2021, Þórunn Arnardóttir, <thar@hi.is>, og Gestur Svavarsson, <gestur@obelisk.is>, eru verkefnastjórar SÍM.

## Grammatek ehf.
[https://www.grammatek.com/](https://www.grammatek.com/)

Tengiliður: Anna Björk Nikulásdóttir, <anna@grammatek.com>

Grammatek ehf. stýrði verkefninu fyrir hönd SÍM fyrstu tvö verkefnisárin. Grammatek sinnir einnig gagna- og hugbúnaðarþróun, fyrst og fremst innan __taltækni og málrýni__.

## Miðeind ehf.
[https://mideind.is/](https://mideind.is/)

Tengiliður: Vilhjálmur Þorsteinsson, <villi@mideind.is>

Miðeind sinnir fjölbreyttri þróun í íslenskri máltækni og tekur þátt í þróun __stoðtóla, málrýnis og vélþýðinga__ innan máltækniáætlunar. Ásamt því að vinna að grunnþróun tekur Miðeind þátt í samstarfsverkefnum við þriðju aðila til þess að prufukeyra afurðir í raunverulegu vinnuumhverfi. 

## Tiro ehf.
[https://tiro.is/](https://tiro.is/)

Tengiliður: Eydís Huld Magnúsdóttir, <eydishm@ru.is>

Tiro vinnur að þróun almennra og sérhæfðra __talgreina fyrir íslensku__ og kemur að ýmsum verkefnum í taltækni innan máltækniáætlunar.

## Ríkisútvarpið ohf.
[https://www.ruv.is/](https://www.ruv.is/)

Tengiliður: Helga Lára Þorsteinsdóttir, <helga.lara.thorsteinsdottir@ruv.is>

RÚV tekur þátt í máltækniáætluninni með því að leggja til aðstöðu til upptöku á röddum fyrir __talgervla__ og veita aðgang að efni úr safni sem nýtist til þróunar á __talgreinum__ fyrir íslensku.

## Creditinfo - Fjölmiðlavaktin ehf.
[https://www.creditinfo.is/lausnir-og-gogn/fjolmidlar.aspx](https://www.creditinfo.is/lausnir-og-gogn/fjolmidlar.aspx)

Tengiliður: Kristín Helga Magnúsdóttir, <kristinh@creditinfo.is>

Hluti af starfsemi Creditinfo - Fjölmiðlavaktarinnar er að rita niður efni úr útvarpi- og sjónvarpi. Þessi reynsla er nýtt innan máltækniáætlunar til þess að rita niður efni á þann hátt sem nýtist við þróun __talgreina__, sem nýtast m.a. í fjölmiðlaumhverfi.

## Blindrafélagið
[https://www.blind.is/](https://www.blind.is/)

Tengiliður: Kristinn Halldór Einarsson, <khe@blind.is>

Blindrafélagið hefur barist fyrir og tekið þátt í að tryggja að íslenska verði stafrænt tungumál. Möguleikar blindra og sjónskertra Íslendinga til að vera samfélagslega virkir í nútíma stafrænu umhverfi standa og falla með því hvort að fullnægjandi máltæknilausnir séu fyrir hendi á íslensku.

## Hljóðbókasafnið
[https://hbs.is/](https://hbs.is/)

Tengiliður: Gunnar Grímsson, <gunnar.grimsson@hbs.is>

Hljóðbókasafnið kemur inn í máltækniáætlunina með mikla reynslu af framleiðslu hljóðbóka á íslensku og skilning á þörfum fólks sem nýtir lesið efni fremur en texta á bók. Safnið hefur lagt til upptökur á efni fyrir þróun __talgervla__ og kemur að mati á gæðum þeirra talgervla sem koma út á vegum áætlunarinnar.