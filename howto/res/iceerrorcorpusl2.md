---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Íslensk L2 villumálheild

Íslenska L2 villumálheildin er safn texta á nútímaíslensku sem hafa verið skrifaðir af annarsmálshöfum íslensku.  
Textarnir hafa verið merktir fyrir villum, t.d. hvað varðar stafsetningu, málfræði og fleira.  

### Heimildir:
[GitLab. Íslensk L2 villumálheild.](https://gitlab.com/icelandic-lt/resources/iceErrorCorpusL2)