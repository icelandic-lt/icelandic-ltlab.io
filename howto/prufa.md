---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Pípa til prufu (e. example pipeline)

Máltækniafurðir eru saman settar af fjölmörgum minni einingum. Allt eftir tilgangi vinnslunnar eru viðeigandi tól valin sem hvert leysa ákveðið verkefni. Hér á eftir fara dæmi um vinnslulínur, sem allar hefjast á grunngreiningu texta.

### Upprunalegur texti
Við ætlum að greina eftirfarandi [texta](https://www.visir.is/g/20212177316d/ser-engan-sjalf-staedan-metnad-fra-is-lenskum-stjorn-voldum) (texti-1):

    Íslensk stjórnvöld hafa ekki sýnt neinn sjálfstæðan metnað um að draga úr losun gróðurhúsalofttegunda, að mati formanns Ungra umhverfissinna sem er staddur
    á COP26-loftslagsráðstefnunni sem hófst í Skotlandi í dag. Ríki heims ræða hvernig þau ætla að ná markmiðum Parísarsamkomulagsins um að halda hnattrænni 
    hlýnun innan 2°C og helst 1,5°C á þessari öld miðað við fyrir iðnbyltingu á COP26-ráðstefnunni.

Og þennan [texta](https://www.visir.is/g/20212176853d/-laeknar-eru-ad-drukkna-i-kliniskri-vinnu-og-hafa-ekki-rymi-til-visinda-starfs-) (texti-2):
    
    Staða okkar í dag er sú að við komumst ekki einu sinni á topp 500 í heiminum á SCImago-listanum. Þar erum við í 644. sæti. Það er hræðilegt. Björn Rúnar 
    segir að sömu sögu sé að segja af öðrum listum. Allt sé á sömu leið: Landspítalinn hefur hrapað niður lista.

### 1. Skipting í setningar og tóka
Mörg máltæknitól vinna setningu fyrir setningu og því er yfirleitt fyrsta skrefið að skipta lengri textum upp í setningar. Virðist í fljótu bragði einfalt verkefni þar sem setningar enda á punkti, spurningarmerki eða upphrópunarmerki (. ? !). Skiptum upphafi texta-2 upp í setningar eftir punktum:

    Staða okkar í dag er sú að við komumst ekki einu sinni á topp 500 í heiminum á SCImago-listanum.
    Þar erum við í 644.
    sæti.
    Það er hræðilegt.

Til þess að geta skipt texta rétt upp í setningar þarf að greina hvenær punktur táknar lok setningar og hvenær hann gegnir öðru hlutverki, t.d. í skammstöfunum, dagsetingum o.s.frv. Þess vegna þarf að þróa **textatilreiðara** fyrir hvert tungumál, sem ræður við að greina þessi atriði. Textatilreiðari máltækniáætlunar [Tokenizer](/howto/nlp/tokenizer) skiptir textum upp í setningar, ofangreint dæmi lítur þá þannig út:

    Staða okkar í dag er sú að við komumst ekki einu sinni á topp 500 í heiminum á SCImago-listanum .
    Þar erum við í 644. sæti .
    Það er hræðilegt .
    Björn Rúnar segir að sömu sögu sé að segja af öðrum listum .
    Allt sé á sömu leið : Landspítalinn hefur hrapað niður lista .

Það sem tilreiðarinn gerir um leið og hann skiptir texta niður í setningar, er að skipta setningunum upp í **tóka**. Eins og sést á dæminu hér að ofan hefur tilreiðarinn bætt inn bili á undan punktum og tvípunkti. Úttak tilreiðarans á tóka-formi getur þá litið svona út:


    ['Staða', 'okkar', 'í', 'dag', 'er', 'sú', 'að', 'við', 'komumst', 'ekki', 'einu', 'sinni', 'á', 'topp', '500', 'í', 'heiminum', 'á', 'SCImago-listanum', '.']
    ['Þar', 'erum', 'við', 'í', '644.', 'sæti', '.']
    ['Það', 'er', 'hræðilegt', '.']
    ['Björn', 'Rúnar', 'segir', 'að', 'sömu', 'sögu', 'sé', 'að', 'segja', 'af', 'öðrum', 'listum', '.']
    ['Allt', 'sé', 'á', 'sömu', 'leið', ':', 'Landspítalinn', 'hefur', 'hrapað', 'niður', 'lista', '.']


### 2. Málfræðileg mörkun
Málfræðileg mörkun (e. PoS-tagging) merkir stök orð með upplýsingum um orðflokk, fall, kyn, tölu o.þ.h. Slíkar upplýsingar eru oft mjög gagnlegar til þess að færa textagreininguna frá því að vinna með ákveðna strengi til almennrar greiningar út frá orðflokkum og slíkum upplýsingum. Ýmis tól krefjast þess einnig að inntakið sé málfræðilega markað (sjá t.d. þáttun hér að neðan). [Málfræðilegur markari](/howto/nlp/pos-tagger) fyrir íslensku gerir þetta fyrir okkur (setning 2 og 3 úr texta-2):

    Þar	aa
    erum	sfg1fn
    við	fp1fn
    í	af
    644.	ta
    sæti	nheþ
    .	pl
    Það	fphen
    er	sfg3en
    hræðilegt	lhensf
    .	pl


### 3. Lemmun
Íslenska er beygingartungumál, þ.e. form orða breytist oft eftir hlutverki þeirra í setningu og/eða tengslum þeirra við önnur orð í setningunni. Sígilda dæmið úr fallbeygingarþjálfuninni, ```hestur```, hefur 15 mismunandi beygingarmyndir (```hestur, hest, hesti, hests, ...```). Í textagreiningu eru orð oft talin á einhverju stigi greiningarinnar. Það getur skipt máli að telja allar beygingarmyndir orðs þannig að þær eigi saman, t.d. þannig að í textanum ```Þessi hestur heitir Gráni. Ég hef aldrei sé fallegri hest.``` teljum við orðið ```hestur``` tvisvar, en ekki ```hestur``` einu sinni og ```hest``` einu sinni. Væri það gert, sæi greininginn ekkert sameignlegt með þessum tveimur setningum. Til þess að geta talið orð út frá grunnformum þeirra (flettimyndum, lemmum) og t.d. unnið tölfræði út frá því eða undirbúið inntak fyrir þjálfun líkana, notum við lemmunarvirknina í markaranum.

    <Lemmatized output>


### 4. Uppfletting í BÍN
Beygingarlýsing íslensks nútímamáls, [BÍN](https://bin.arnastofnun.is/), hefur verið geysivinsæll uppflettivefur um beygingar íslenskra orða um árabil. Innan máltækniáætlunar hefur vinna við BÍN haldið áfram, með það að markmiði að auka notagildi hennar enn í máltækni. Gagnagrunnurinn hefur nú verið gerður einstaklega léttur í meðförum með [BINpackage](https://github.com/mideind/BinPackage). Mörg máltæknitól nýta sér nú þegar að fletta upp í BÍN til þess að hjálpa við til við greiningar, finna mögulega orðflokka, beygingar eða til þess að aðstoða við að meta líkurnar á því að orð sé gilt í íslensku. Eftirfarandi dæmi sýnir mögulega orðflokka orða ásamt grunnmynd:

    Íslensk: {('íslenskur', 'lo')}
    stjórnvöld: {('stjórnvald', 'hk'), ('stjórnvöld', 'hk')}
    hafa: {('haf', 'hk'), ('hafa', 'so')}
    ekki: {('ekki', 'ao'), ('ekki', 'kk')}
    sýnt: {('sýnn', 'lo'), ('sýna', 'so'), ('sýndur', 'lo')}
    neinn: {('neinn', 'fn')}
    sjálfstæðan: {('sjálfstæður', 'lo')}
    metnað: {('metnaður', 'kk'), ('metna', 'so')}
    um: {('um', 'st'), ('um', 'fs'), ('um', 'ao')}
    að: {('að', 'fs'), ('að', 'ao'), ('að', 'nhm'), ('að', 'st')}
    draga: {('draga', 'so'), ('drag', 'hk'), ('draga', 'kvk'), ('dragi', 'kk')}
    úr: {('úr', 'hk'), ('úr', 'kk'), ('úr', 'ao'), ('úr', 'fs')}
    losun: {('losun', 'kvk')}
    gróðurhúsalofttegunda: {('gróðurhúsalofttegund', 'kvk')}


### 5. Setningagreining, þáttun
Oft er grunn textagreiningu lokið með tilreiðingu, mörkun og/eða lemmun, með upplýsingar sem þar fást er t.d. hægt að þjálfa alls kyns tölfræðileg líkön og tauganet. Frekari upplýsingar um strúktur setninga, sem fæst með þáttun (e. parsing) koma þó oft að miklu gagni t.d. í málrýni. Máltækniáætlunin skilar tveimur gerðum þáttara: [djúpþáttarinn Greynir](https://github.com/mideind/GreynirPackage), sem fullþáttar setningar og sýnir hvernig allir liðir hennar tengjast, og svo [grunnþáttarinn IceParser](https://github.com/hrafnl/icenlp), sem skiptir setningunni upp í liði, án þess að sýna hvernig þeir tengjast innbyrðis. Grunnþáttarinn getur einnig sýnt setningahlutverk einstakra liða, þ.e. frumlag, sagnlið, andlag.

Djúpþáttun ```Það er hræðilegt.```` (á [vefsíðu Greynis](https://greynir.is/) er hægt að sjá myndræna framsetningu á setningatrjám)

    S0
    +-S-MAIN
      +-IP
        +-NP-SUBJ
          +-pfn_hk_et_nf: 'Það'
        +-VP
          +-VP-AUX
            +-so_et_p3: 'er'
          +-NP-PRD
            +-lo_sb_nf_et_hk: 'hræðilegt'
    +-'.'


Grunnþáttun ```Staða okkar í dag er sú að við komumst ekki einu sinni á topp 500 í heiminum á SCImago-listanum```

    {*SUBJ> [NP Staða nven ] {*QUAL [NP okkar fp1fe ] } }
    [PP í ao [NP dag nkeo ] ]
    [VPb er sfg3en ]
    {*COMP< [NP sú faven ] }
    [SCP að c ]
    {*SUBJ> [NP við fp1fn ] }
    [VP komumst sfm1fþ ]
    [AdvP ekki aa ]
    [MWE_AdvP einu foheþ sinni nheþ ]
    [PP á ao [NP topp nkeo 500 ta ] ]
    [PP í aþ [NP heiminum nkeþg ] ]
    [PP á aþ [NP SCImago-listanum nkeþgs ] ]
    . .


### 7. Málrýni
Málrýni, eða stafsetningar- og málfræðileiðréttingahugbúnaður, nýtist fólki við öll almenn skrif. Einnig getur málrýni komið að gagni í öðrum máltæknihugbúnaði, til dæmis til þess að leiðrétta leitarorð fyrir leitarvélar. Dæmi: ```Björn Rúnar segjir að sömu sögu sé að segja af öðrum litsum```

    Björn Rúnar 
    segir      Orðið 'segjir' var leiðrétt í 'segir'
    að         
    sömu       
    sögu       
    sé         
    að         
    segja      
    af         
    öðrum      
    listum     Orðið 'litsum' var leiðrétt í 'listum'
    .          

<!--### 8. Nafnakennsl (NER)

(verður uppfært)

### 9. Orðagreypingar - skyld orð

(verður uppfært) -->

### 8. Vélþýðingar

Sjálfvirkar þýðingar nýtast sérlega vel til þess að gefa hugmynd um efni texta á öðru tungumáli og eins til þess að flýta fyrir vinnu þýðenda. Á vefsíðunni [Vélþýðing](https://velthyding.is/) er hægt að prófa þýðingar milli íslensku og ensku, en uppsetning og keyrsla á vélþýðingarlíkönum krefst mikils reikniafls og tilraunir því best framkvæmdar gegnum vefþjónustu.

    Íslensk stjórnvöld hafa ekki sýnt neinn sjálfstæðan metnað um að draga úr losun gróðurhúsalofttegunda, að mati formanns Ungra umhverfissinna sem er staddur 
    á COP26-loftslagsráðstefnunni sem hófst í Skotlandi í dag. 
    Ríki heims ræða hvernig þau ætla að ná markmiðum Parísarsamkomulagsins um að halda hnattrænni hlýnun innan 2°C og helst 1,5°C á þessari öld miðað við fyrir 
    iðnbyltingu á COP26-ráðstefnunni.

    The Icelandic government has shown no independent ambition to reduce greenhouse gas emissions, according to the chairman of the Young Environment 
    at the COP26 climate conference which started today in Scotland.
    World States Discuss How They Will Reach Paris Agreement Goals to Keep Global warming within 2°C and preferably 1.5°C this century 
    compared to pre-industrial times at the COP26.


### 9. Talgreining

Með talgreiningu breytum við tali í texta. Uppsetning á talgreiningarþjónustu krefst nokkurrar kunnáttu en hér er hægt að prófa [íslenskan talgreini](https://tal.ru.is/). 

### 10. Talgerving

Með talgervingu breytum við texta í tal. Uppsetning á talgervingarþjónustu krefst nokkurrar kunnáttu en hér er hægt að prófa [íslenskan talgervil](https://tiro.is/talgerving)
