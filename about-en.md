---
title: SÍM 
subtitle: Consortium for Icelandic Language Technology
layout: page
show_sidebar: false
---

## The Árni Magnússon Institute for Icelandic Studies
[https://arnastofnun.is/](https://arnastofnun.is/en/front-page)

Contact: Steinþór Steingrímsson, <steinthor.steingrimsson@arnastofnun.is>

The Árni Magnússon Institute is the center for __language resources for Icelandic__, where work is carried out on various corpora and on the development of lexical data for Icelandic and Icelandic LT. The development of language resources is the institutes main role within the LT-programme, but it is also involved in the development of support tools and in supplying advice, e.g. in licensing matters.

## Reykjavik University
[https://lvl.ru.is/](https://lvl.ru.is/)

Contacts: Jón Guðnason, <jg@ru.is> and Hrafn Loftsson, <hrafn@ru.is>

Reykjavik University hosts the Language and Voice Lab which conducts research on speech and language technology. The Language and Voice Lab's involvement in the LT-programme can be divided into two parts: Research and development of __speech technology__ and research and development related to texts, primarily __support tools and machine translations__. RU manages the collection of speech data for speech recognition and speech synthesis for Icelandic, e.g. through Samrómur. Reykjavik University offers master's and Ph. D. studies in language technology, in collaboration with the University of Iceland.

## The University of Iceland
[http://linguist.is/language-and-technology-lab/](http://linguist.is/language-and-technology-lab/)

Contact: Anton Karl Ingason, <antoni@hi.is>

Within the University of Iceland, various research in language technology is carried out. The University's involvement in LT is related primarily to __spell and grammar checking__, gathering and analyzing data on spelling and grammar errors in texts and software development. The University of Iceland offers master's and Ph. D. studies in language technology, in collaboration with Reykjavik University.

## Grammatek ehf.
[https://www.grammatek.com/](https://www.grammatek.com/)

Contact: Anna Björk Nikulásdóttir, <anna@grammatek.com>

Grammatek ehf. takes the role of __project lead__ on behalf of SÍM and takes the role of a liaison with Almannarómur, the industry and anyone who may be interested in LT for Icelandic and how the products of the programme may be utilized. Grammatek also handles data and software development, primarily in __speech technology and spell and grammar checking__.

## Miðeind ehf.
[https://mideind.is/](https://mideind.is/)

Contact: Vilhjálmur Þorsteinsson, <villi@mideind.is>

Miðeind handles various developments in Icelandic LT and takes part in the development of __support tools, spell and grammar checking, and machine translation__ within the LT-programme. In addition to basic development Miðeind takes part in collaboration projects with third parties to test drive products in a real work environment.

## Tiro ehf.
[https://tiro.is/](https://tiro.is/)

Contact: Eydís Huld Magnúsdóttir, <eydishm@ru.is>

Tiro works towards the development of general purpose and specialized __speech synthesizers for Icelandic__ and is involved in various projects in speech technology within the LT-programme.

## RÚV - The Icelandic National Broadcasting Service
[https://www.ruv.is/](https://www.ruv.is/)

Contact: Helga Lára Þorsteinsdóttir, <helga.lara.thorsteinsdottir@ruv.is>

RÚV participates in the LT-programme by providing facilities for recording voices for __speech synthesizers__ and providing access to material from their collection that is utilized in the development of __speech recognizers__ for Icelandic.

## Creditinfo - Fjölmiðlavaktin ehf.
[https://www.creditinfo.is/lausnir-og-gogn/fjolmidlar.aspx](https://www.creditinfo.is/lausnir-og-gogn/fjolmidlar.aspx)

Contact: Kristín Helga Magnúsdóttir, <kristinh@creditinfo.is>

One part of the work of Creditinfo - Fjölmiðlavaktin is writing down material from radio and television. This experience is utilized within the LT-programme to write down material in a way that is usable in the development of __speech recognizers__, that are useful e.g. in news media environments.

## Blindrafélagið
[https://www.blind.is/](https://www.blind.is/en)

Contact: Kristinn Halldór Einarsson, <khe@blind.is>

Blindrafelagid, Icelandic Association of the Visually Impaired (BIAVI) has fought for, and participated in ensuring that Icelandic becomes a digital language. The possibilities for blind and visually impaired to be socially active in a modern digital environment rely on adequate LT solutions being available for Icelandic.

## Hljóðbókasafnið
[https://hbs.is/](https://hbs.is/)

Contact: Gunnar Grímsson, <gunnar.grimsson@hbs.is>

Hljóðbókasafnið (The Audiobook Library of Iceland) enters the LT-programme with extensive experience in the production of audio books in Icelandic and knowledge of the needs of people who listen to books rather than read them. The Library has provided recordings of material for the development of __speech synthesizers__ and is involved in evaluating the quality of synthesizers that are published by the programme.