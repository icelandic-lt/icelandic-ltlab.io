---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Thrax g2p
Líkan rittákns-til-fónems, oftast kallað g2p-líkan (e. grapheme to phoneme model) er vörpun rittákna yfir í [fónem (stundum kallað hljóðan)](https://is.wikipedia.org/wiki/Hljóðan).  
Þetta er svo notað af talgervlum.  
Thrax er tólasett til að byggja upp málfræði, og notar OpenFST fyrir stöðuferjöld (e. FST, Finite-state transducers).[^1]

### Uppsetning:
Til að setja tólið upp er mögulega best að nota [Docker (sjá leiðbeiningar fyrir uppsetningu Docker á hinum ýmsu stýrikerfum)](https://www.docker.com/get-started).  

Hægt er að sækja thrax-g2p tólið beint af [GitLab síðunni](https://gitlab.com/icelandic-lt/tts/g2p-thrax), eða með skipanalínutólinu `git`:

`git clone https://gitlab.com/icelandic-lt/tts/g2p-thrax.git`  

og byggja svo docker myndina:  

`docker build -t "g2p-thrax:latest" .`  

### Heimildir:
[^1]: [Thrax README. OpenGRM](http://www.opengrm.org/twiki/pub/GRM/ThraxDownload/README)  
[G2P-Thrax. GitLab](https://gitlab.com/icelandic-lt/tts/g2p-thrax)  
