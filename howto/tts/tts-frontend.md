---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Textaforvinnsla fyrir talgervingu

Textaforvinnsla fyrir talgervingu fer fram í nokkrum skrefum: hreinsun og tilreiðing, textanormun, mögulega stafsetningarleiðrétting og innsetning þagna, og svo hljóðritun.

TTS-frontend pípan framkvæmir textaforvinnslu fyrir íslensku og hægt er að nota hana til þess að undirbúa hljóðbækur fyrir talgervilslestur. 

Hægt er að nota stök tól beint eins og textanormun og hljóðritun, en pakkinn **TTS-Frontend** tengir öll tólin í eina pípu. Vefþjónusta til þess að nota með talgervilsþjóni er undir **TTS-Frontend-Service**. 

Forvinnsla á einfaldri setningu:

    Það voru 4 km eftir, sagði þjálfari KR.

Normaður texti (`<sil>` merkir að talgervillinn eigi að gera stutta pásu):

	Það voru fjórir kílómetrar eftir <sil> sagði þjálfari K R

Hljóðritaður texti:

    T a: D v O: r Y f j ou: r I r c_h i: l ou m E t r a r E f t I r <sil> s a G D I T j au l v a r I k_h au: E r


### Tenglar:
[GitLab. tts-frontend.](https://gitlab.com/icelandic-lt/tts/tts-frontend)

[GitLab. tts-frontend-service.](https://gitlab.com/icelandic-lt/tts/tts-frontend-service)

[GitLab. text-cleaner.](https://gitlab.com/icelandic-lt/tts/text-cleaner)

[GitLab. regina-normalizer.](https://gitlab.com/icelandic-lt/tts/regina-normalizer)

[GitLab. phrasing-tool.](https://gitlab.com/icelandic-lt/tts/phrasing-tool)

[GitLab. IceG2P.](https://gitlab.com/icelandic-lt/tts/ice-g2p)