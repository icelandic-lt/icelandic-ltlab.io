---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Íslenskar sérhæfðar villumálheildir

Íslensku sérhæfðu villumáheildirnar eru þrjár talsins:
- Íslenska L2 villumálheildin
- Íslenska lesblinduvillumálheildin
- Íslenska barnamálsvillumálheildin.  

Allir textar í öllum þremur villumálheildum hafa verið merktir fyrir villum, t.d. hvað varðar stafsetningu, málfræði og fleira.  
Villumálheildir eru notaðar til að finna villur í rituðum texta, t.d. í villuleitarforritum.  
Öll skjöl í málheildinni eru á xml ([Extensible Markup Language](https://en.wikipedia.org/wiki/XML)) formi.  

### Dæmi:
![iceerrorcorpus](/assets/img/iceerrorcorpus-img1.png)  
Í þessu dæmi úr [Íslensku villumálheildinni (IceEC)](/howto/res/iceerrorcorpus) má sjá að í upprunalegum (<span style="color:blue">original</span>) texta er eintala þar sem á að vera fleirtala (<span style="color:red">singular4plural</span>), og leiðrétt orð (<span style="color:green">corrected</span>).  

### Heimildir:
[GitLab. IceErrorCorpusSpecialized.](https://gitlab.com/icelandic-lt/resources/iceerrorcorpusspezialized)