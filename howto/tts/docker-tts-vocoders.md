---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Docker talgervlar

Talgervilskerfi sem notar STRAIGHT kerfið.  

### Heimildir:
[GitLab. Docker Tts Vocoders.](https://gitlab.com/icelandic-lt/tts/docker-tts-vocoders)  