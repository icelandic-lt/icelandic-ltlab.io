---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# PEDI - Ritstýringartól fyrir framburðarorðabækur

Pedi (A **P**ronunciation Dictionary **Edi**tor) er ritstýringartól fyrir framburðarorðabækur.
Með því er fljótlegt að yfirfara hljóðritanir og aðrar orðfræðiupplýsingar í orðabókum, og eins er hægt að spila hljóðritanir hvers orðs með talgervli.


### Tenglar:
[GitLab. Pedi.](https://gitlab.com/icelandic-lt/resources/pedi)