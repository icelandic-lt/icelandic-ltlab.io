---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Leiðbeiningar

- [Pípa til prufu (e. example pipeline)](/howto/prufa)

### Málföng
- [GreynirCorpus](/howto/res/greynircorpus)
- [Íslensk orðskiptingarorðabók](/howto/res/hyphenation-is)
- [Íslenskar villumálheildir](/howto/res/iceerrorcorpus)
- [Íslensk framburðarorðabók fyrir máltækni](/howto/res/iceprondict)
- [IceTaboo](/howto/res/icetaboo)
- [Greypingar](/howto/res/greypingar-profunarsett)
- [Pedi](/howto/res/pedi)

### Málrýni
- [GreynirCorrect](/howto/checker/greynircorrect)
- [Yfirlestur.is](/howto/checker/yfirlestur)

### Stoðtól
- [GreynirPackage](/howto/nlp/greynirpackage)
- [IceNLP](/howto/nlp/icenlp)
- [NER](/howto/nlp/ner)
- [Orðtökutól](/howto/nlp/ordtaka)
- [Málfræðilegur markari](/howto/nlp/pos-tagger)
- [Skiptir](/howto/nlp/skiptir)
- [Tokenizer](/howto/nlp/tokenizer)

### Talgerving
<!--- [Docker talgervlar](/howto/tts/docker-tts-vocoders)
- [FastSpeech2](/howto/tts/fastspeech2)
- [G2p LSTM](/howto/tts/g2p-lstm)
- [G2p Thrax](/howto/tts/g2p-thrax)
- [regina normalizer](/howto/tts/regina-normalizer)
- [Unit Selection Festival](/howto/tts/unit-selection-festival)-->
- [WebRICE](/howto/tts/webrice)
- [Textaforvinnsla](/howto/tts/tts-frontend)
- [LOBE](/howto/tts/lobe)

<!--### Talgreining
- [Samröðun og bútun](/howto/asr/alignment-and-segmentation)
- [Samrómur](/howto/asr/samromur)
- [Talgreining fyrir Samróm](/howto/asr/samromur-asr)
- [Subword Perplexity Tests](/howto/asr/subword-perplexity-tests)

### Vélþýðingar
- [GreynirT2T](/howto/mt/greynirt2t)
- [Nnserver](/howto/mt/nnserver)
- [Moses PBSMT](/howto/mt/smt)
- [Vélþýðing](/howto/mt/velthyding)-->
