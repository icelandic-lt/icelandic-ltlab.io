---
title: Ritaskrá
subtitle: Ritrýndar greinar um máltækni eftir þátttakendur í SÍM
layout: page
show_sidebar: false
---

# 2020

Anna Björk Nikulásdóttir, Jón Guðnason, Anton Karl Ingason, Hrafn Loftsson, Eiríkur Rögnvaldsson, Einar Freyr Sigurðsson, Steinþór Steingrímsson (2020): __Language Technology Programme for Icelandic 2019-2023.__ In: Proceedings of the 12th Conference on Language Resources and Evaluation (LREC 2020), pp. 3414-3422. [pdf](https://www.aclweb.org/anthology/2020.lrec-1.418.pdf)

Atli Þór Sigurgeirsson, Gunnar Thor Örnólfsson og Jón Guðnason (2020): __Manual Speech Synthesis Data Acquisition – From Script Design to Recording Speech.__ In proceedings of SLTU-CCURL 2020. [pdf](https://languageandvoice.files.wordpress.com/2020/05/sigurgeirsson2020sltuccurl-manual_speech_synthesis_data_aquisition.pdf)

David E. Mollberg, Ólafur H. Jónsson, Sunneva Þorsteinsdóttir, Steinþór Steingrímsson, Eydís Huld Magnusdóttir og Jón Guðnason (2020): __Samrómur: Crowd-sourced Data Collection for Icelandic Speech Recognition.__ In proceedings of LREC 2020. [pdf](https://languageandvoice.files.wordpress.com/2020/05/mollberg2020lrec-samromur.pdf)

Haukur Páll Jónsson, Haukur Barri Símonarson, Vésteinn Snæbjarnason, Steinþór Steingrímsson og Hrafn Loftsson (2020): __Experimenting with Different Machine Translation Models in Medium-Resource Settings.__ Accepted to the 23rd International Conference on Text, Speech and Dialogue (TSD 2020). Brno, Czech Republic.

Jón Friðrik Daðason, David E. Mollberg og Hrafn Loftsson (2020): __Kvistur 2.0: a BiLSTM Compound Splitter for Icelandic__ in proceedings of LREC 2020. [pdf](https://arxiv.org/pdf/2004.07776.pdf)

Steinþór Steingrímsson, Hrafn Loftsson og Andy Way (2020): __Effectively Aligning and Filtering Parallel Corpora under Sparse Data Conditions.__ In Proceedings of the ACL Student Research Workshop. Seattle, Washington. [pdf](https://www.aclweb.org/anthology/2020.acl-srw.25/)

Svanhvít Lilja Ingólfsdóttir, Ásmundur Alma Guðjónsson og Hrafn Loftsson (2020): __Named Entity Recognition for Icelandic: Annotated Corpus and Models.__ Accepted to the 8th International Conference on Statistical Language and Speech Processing (SLSP 2020). Cardiff, United Kingdom.


# 2019

Anna Björk Nikulásdóttir og Jón Guðnason (2019): __Bootstrapping a Text Normalization System for an Inflected Language. Numbers as a Test Case.__ In proceedings of Interspeech 2019. Graz, Austria. [pdf](https://languageandvoice.files.wordpress.com/2019/07/nikulasdottir2019interspeech-text_normalization_for_inflected_languages.pdf)

Anna Vigdís Rúnarsdóttir, Inga Rún Helgadóttir og Jón Guðnason (2019): __Lattice re-scoring during manual editing for automatic error correction of ASR transcripts.__ In proceedings of Interspeech 2019. Graz, Austria. [pdf](https://languageandvoice.files.wordpress.com/2019/07/runarsdottir2019interspeech-rescoring_word_lattices_from_asr_final.pdf) 

Inga Rún Helgadóttir, Anna Björk Nikulásdóttir, Michal Borský, Judy Fong, Róbert Kjaran og Jón Guðnason (2019): __The Althingi ASR System.__ In proceedings of Interspeech 2019. Graz, Austria. [pdf](https://languageandvoice.files.wordpress.com/2019/07/helgadottir2019interspeech-the_althingi_asr_system.pdf)

Kristín Bjarnadóttir, Kristín Ingibjörg Hlynsdóttir og Steinþór Steingrímsson (2019): __Dim: The Database of Icelandic Morphology.__ NoDaLiDa 2019 Turku, Finnlandi, 30. september - 2. október 2019. [pdf](https://www.aclweb.org/anthology/W19-6116/)

Starkaður Barkarson og Steinþór Steingrímsson (2019): __Compiling and Filtering ParIce:An English-Icelandic Parallel Corpus.__ NoDaLiDa 2019 Turku, Finnlandi, 30. september - 2. október 2019. [pdf](https://www.aclweb.org/anthology/W19-6115/)

Steinþór Steingrímsson, Örvar Kárason og Hrafn Loftsson (2019): __Augmenting a BiLSTM tagger with a Morphological Lexicon and a Lexical Category Identification Step.__ In proceedings of Recent Advances in Natural Language Processing (RANLP 2019). [pdf](https://arxiv.org/abs/1907.09038)

Steinþór Steingrímsson (2019): __Risamálheildin.__ Orð og tunga. (21), Stofnun Árna Magnússonar í íslenskum fræðum. 159-168. [pdf](https://ordogtunga.arnastofnun.is/index.php/ord-og-tunga/article/view/12/3)

Svanhvít Lilja Ingólfsdóttir, Hrafn Loftsson, Jón Friðrik Daðason og Kristín Bjarnadóttir (2019): __Nefnir: A high accuracy lemmatizer for Icelandic.__ In proceedings of NoDaLiDa-2019. [pdf](https://www.aclweb.org/anthology/W19-6133/) 

Svanhvít Lilja Ingólfsdóttir, Sigurjón Þorsteinsson og Hrafn Loftsson (2019): __Towards High Accuracy Named Entity Recognition for Icelandic__. In proceedings of NoDaLiDa-2019. [pdf](https://www.aclweb.org/anthology/W19-6142/)

Vilhjálmur Þorsteinsson, Hulda Óladóttir og Hrafn Loftsson (2019): __A Wide-Coverage Context-Free Grammar for Icelandic and an Accompanying Parsing System.__ In proceedings of Recent Advances in Natural Language Processing (RANLP 2019). [pdf](https://www.aclweb.org/anthology/R19-1160.pdf)

# 2018

Anna Björk Nikulásdóttir, Inga Rún Helgadóttir, Matthías Pétursson og Jón Guðnason (2018): __Open ASR for Icelandic: Resources and a Baseline System.__ In proceedings of LREC 2018. Myazaki, Japan. [pdf](https://languageandvoice.files.wordpress.com/2018/03/nikulasdottir2018lrec_open-asr-icelandic_final.pdf)

Anna Björk Nikulásdóttir, Jón Guðnason og Eiríkur Rögnvaldsson (2018): __An Icelandic Pronunciation Dictionary for TTS.__ In proceedings of SLT 2018. [pdf](https://languageandvoice.files.wordpress.com/2019/01/nikulasdottier2018sltispronunciationdictionarytts.pdf)

Steinþór Steingrímsson, Sigrún Helgadóttir, Eiríkur Rögnvaldsson, Starkaður Barkarson og Jón Guðnason (2018): __Risamálheild: A Very Large Icelandic Text Corpus.__ Proceedings of LREC 2018. Myazaki, Japan. 4361-4366.

