---
title: SÍM
subtitle: Consortium for Icelandic Language Technology
layout: page
show_sidebar: false
---

# Tutorials (in Icelandic)

- [Example pipeline)](/howto/prufa)

### Language Resources
- [GreynirCorpus](/howto/res/greynircorpus)
- [Greypingar prófunarsett](/howto/res/greypingar-profunarsett)
- [Icelandic Hyphenation Dictionary](/howto/res/hyphenation-is)
- [Icelandic Error Corpus (IceEC)](/howto/res/iceerrorcorpus)
- [Icelandic L2 Error Corpus](/howto/res/iceerrorcorpusl2)
- [Icelandic Specialized Error Corpora](/howto/res/iceerrorcorpusspecialized)
- [Icelandic Pronunciation Dictionary for Language Technology](/howto/res/iceprondict)
- [IceTaboo](/howto/res/icetaboo)
- [Embeddings](/howto/res/ordgreypingar_embeddings)
- [Pedi](/howto/res/pedi)

### Spell & grammar checking
- [GreynirCorrect](/howto/checker/greynircorrect)
- [Yfirlestur.is](/howto/checker/yfirlestur)

### Support tools / NLP
- [GreynirPackage](/howto/nlp/greynirpackage)
- [GreynirSeq](/howto/nlp/greynirseq)
- [IceNLP](/howto/nlp/icenlp)
- [NER](/howto/nlp/ner)
- [Orðtökutól](/howto/nlp/ordtaka)
- [ParsingTestPipe](/howto/nlp/parsingtestpipe)
- [POS Tagger](/howto/nlp/pos-tagger)
- [Skiptir](/howto/nlp/skiptir)
- [Tokenizer](/howto/nlp/tokenizer)

### Speech synthesis (text to speech)
- [Docker TTS vocoders](/howto/tts/docker-tts-vocoders)
- [FastSpeech2](/howto/tts/fastspeech2)
- [G2p LSTM](/howto/tts/g2p-lstm)
- [G2p Thrax](/howto/tts/g2p-thrax)
- [regina normalizer](/howto/tts/regina-normalizer)
- [LOBE](/howto/tts/lobe)
- [Tiro-TTS](/howto/tts/tiro-tts)
- [Unit Selection Festival](/howto/tts/unit-selection-festival)
- [WebRICE](/howto/tts/webrice)

### ASR (Automatic speech recognition)
- [Alignment and segmentation](/howto/asr/alignment-and-segmentation)
- [Samrómur](/howto/asr/samromur)
- [Samrómur ASR](/howto/asr/samromur-asr)
- [Subword Perplexity Tests](/howto/asr/subword-perplexity-tests)

### MT (Machine translation)
- [GreynirT2T](/howto/mt/greynirt2t)
- [Nnserver](/howto/mt/nnserver)
- [Moses PBSMT](/howto/mt/smt)
- [Vélþýðing](/howto/mt/velthyding)
