---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# IceTaboo

IceTaboo er málheild/listi íslenskra orða sem **gætu** talist óviðeigandi og/eða verið gildishlaðin á einhvern hátt, stundum kölluð bannorð.  
Skrárnar eru á `.tsv` formi.  

##### Dæmi úr listanum:
![dæmi um bannorð](/assets/img/icetaboo-img1.png)  

Listann má nota til að taka út orð sem þykja óæskileg fyrir börn, t.d. í leikjum.  

### Heimildir:
[GitLab. IceTaboo.](https://gitlab.com/icelandic-lt/resources/iceTaboo#listi-yfir-%C3%B3vi%C3%B0eigandi-or%C3%B0)