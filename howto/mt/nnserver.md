---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Nnserver

Þessi þýðingarlíkön voru þróuð með [GreynirT2T](/howto/mt/greynirt2t) en verða ekki þróuð lengur, þar sem [Tensor2Tensor](https://github.com/tensorflow/tensor2tensor) sem þau byggja á er ekki lengur í þróun.  
Áframhaldandi þróun á talgreiningu fyrir íslensku verður með [GreynirSeq](/howto/nlp/greynirseq).  

### Heimildir:
[GitLab. Nnserver.](https://gitlab.com/icelandic-lt/mt/nnserver)