---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# LOBE

LOBE er upptökuþjónn gerður sérstaklega fyrir gagnaöflun fyrir talgervla. Hann styður mörg söfn, einn eða fleiri mælendur, og getur prompt sentences based on phonetic coverage.  

### Tenglar:
[GitLab. LOBE.](https://gitlab.com/icelandic-lt/tts/LOBE)