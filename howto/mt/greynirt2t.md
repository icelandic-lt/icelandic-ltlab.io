---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# GreynirT2T

GreynirT2T er vélþýðingarkerfi milli íslensku og ensku byggt á [Tensor2Tensor](https://github.com/tensorflow/tensor2tensor), en það er ekki lengur í þróun.  
Áframhaldandi þróun á talgreiningu fyrir íslensku verður með [GreynirSeq](/howto/nlp/greynirseq).  

### Heimildir:
[GitLab. GreynirT2T.](https://gitlab.com/icelandic-lt/mt/GreynirT2T)