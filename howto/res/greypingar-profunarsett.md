---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Greypingar: Prófunarsett og gagnasöfn

Þessi hirsla hýsir gagnasöfn sem meta getu orðvigralíkana til þess að átta sig á merkingarfræði og orðhlutafræði íslensku, auk forþjálfaðra greypinga sem hafa verið metnar með téðum gagnasöfnum. 
 
Prófunarsettið er annarsvegar þýðing á [Multi-SimLex](https://arxiv.org/abs/2003.04866), og hinsvegar íslenskt beygingar- og afleiðsluprófunarsett byggt á BATS [(Bigger Analogy Test Set)](https://aclanthology.org/N16-2002.pdf).  

## Hvað er orðagreyping?

Orðagreyping (e. [word embedding](https://en.wikipedia.org/wiki/Word_embedding)) er framsetning orða sem vigra. Vigrarnir eru búnir til með því að keyra til þess gerð algrím á miklu magni texta og niðurstaðan verður sú að orð sem notuð eru á svipaðan hátt fá svipaða framsetningu. Vigrar skyldra eða tengdra orða hafa því svipuð gildi en einnig lýsa sambærileg vensl milli orða sér oft á svipaðan hátt. Þannig má t.d. greina vensl ```land - höfuðborg``` út frá því að orð eins og ```Frakkland - París, Þýskaland - Berlín, Ítalía - Róm``` sýna sambærileg tengsl í vigurrúminu. 

Orðagreypingar nýtast því í greiningu texta af ýmsu tagi þar sem hægt er að greina merkingu og vensl út frá tölulegri framsetningu orðaforðans.

### Tenglar:
[Ordgreypingar_embeddings, GitLab](https://gitlab.com/icelandic-lt/resources/ordgreypingar_embeddings/-/blob/main/LESTU.md)
[Greypingar Profunarsett, GitLab](https://gitlab.com/icelandic-lt/resources/greypingar_profunarsett)