---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# FastSpeech 2 - PyTorch útfærsla

Þetta er PyTorch útfærsla á talgervilskerfinu [FastSpeech 2](https://arxiv.org/abs/2006.04558) frá Microsoft, og byggt á [útfærslu xcmyz](https://github.com/xcmyz/FastSpeech).  

### Heimildir:
[GitLab. FastSpeech2.](https://gitlab.com/icelandic-lt/tts/FastSpeech2)