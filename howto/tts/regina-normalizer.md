---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Textanormun og regina-normalizer

Textanormun er ferli sem felur í sér undirbúning texta fyrir hljóðritun, og þar með fyrir talgervil.  

regina-normalizer er tól til textanormunar fyrir íslensku.  

### Uppsetning og notkun:

Málfræðilegi markarinn POS þarf að vera uppsettur ([sjá leiðbeiningar](/howto/nlp/pos-tagger)).  

Hægt er að sækja regina-normalizer beint af [GitLab síðunni](https://gitlab.com/icelandic-lt/tts/regina-normalizer), eða með skipanalínutólinu `git`:

`git clone https://gitlab.com/icelandic-lt/tts/regina-normalizer.git`  

Svo setjum við tólum upp:  
`pip install -e .`

Svo er það keyrt t.d. svona:
`python3 -W ignore -m regina_normalizer.main "62 m/s!" other`  
![dæmi 1](/assets/img/regina-normalizer-example1.png)  

### Heimildir:
[GitLab. regina-normalizer.](https://gitlab.com/icelandic-lt/tts/regina-normalizer)