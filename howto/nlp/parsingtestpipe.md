---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# ParsingTestPipe

ParsingTestPipe er prófunarsvíta þróuð af [Miðeind](https://mideind.is/) sem notar gullstaðal [GreynirCorpus](https://gitlab.com/icelandic-lt/resources/GreynirCorpus#user-content-st%C3%B3r-trj%C3%A1banki-me%C3%B0-%C3%BE%C3%A1ttu%C3%B0um-%C3%ADslenskum-texta) til að mæla árangur Greynisþáttarans. Hún notar [evalb](https://nlp.cs.nyu.edu/evalb/) og [IceNLP](/howto/nlp/icenlp).  

### Heimildir:
[ParsingTestPipe. GitLab](https://gitlab.com/icelandic-lt/nlp/ParsingTestPipe)