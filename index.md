---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Máltækni fyrir íslensku

Samstarf um íslenska máltækni (SÍM) er hópur íslenskra háskóla, stofnana, fyrirtækja og félagasamtaka sem vinnur að rannsóknum og þróun innan áætlunarinnar [Máltækni fyrir íslensku 2019-2023](https://www.stjornarradid.is/library/03-Verkefni/Menningarmal/M%C3%A1lt%C3%A6kni%C3%A1%C3%A6tlun.pdf). SÍM vinnur samkvæmt samningi við [Almannaróm](https://almannaromur.is/) að þróun kjarnaverkefna í máltækni og skilar afurðum (málföngum, hugbúnaði og kóða) inn til [CLARIN á Íslandi](https://clarin.is/).

Nánari upplýsingar um þátttakendur í samstarfinu má finna undir flipanum "Um SÍM", en Þórunn Arnardóttir og Gestur Svavarsson, verkefnastjórar SÍM, svara öllum fyrirspurnum varðandi verkefnið og samstarfið: <thar@hi.is>, <gestur@obelisk.is>.

Hér má finna allar hugbúnaðarhirslur sem tilheyra afurðum verkefnisins, allur hugbúnaður er gefinn út með opnum leyfum. 

## Yfirlit yfir kjarnaverkefni máltækniáætlunar 
Innan máltækniáætlunarinnar eru skilgreind 6 kjarnaverkefni, sem eiga að leggja grunninn að þróun máltæknilausna fyrir íslensku. Hér má finna tengla á allar hugbúnaðarhirslur kjarnaverkefnanna sem hefur verið skilað hingað til í verkefninu:

[Yfirlit yfir hugbúnaðarhirslur](https://gitlab.com/icelandic-lt)

### Málföng
Öll máltækni byggist á málgögnum: textum og/eða hljóðupptökum. Þau eru nauðsynleg við greiningu máls, söfnun orðaforða og til að finna reglur og mynstur í tungumálinu. Út frá málgögnum er þannig hægt að "kenna" tölvunum það sem máli skiptir fyrir þann hugbúnað sem verið er að þróa, eða láta hugbúnað finna reglur og mynstur í miklu magni gagna. Innan máltækniáætlunar er unnið að stórum textasöfnum og þau undirbúin fyrir notkun í máltækni, bæði einmála íslensk textasöfn og tvímála samhliða málheildir sem innihalda íslenska og enska texta. Einnig fara fram upptökur á tali í miklu magni, bæði í gegnum lýðvirkjunarverkefnið [Samróm](https://samromur.is/) en einnig hágæða upptökur í hljóðveri fyrir þróun talgervla.
Jafnframt er unnið að gagnasöfnum sem geyma upplýsingar um einstaka þætti tungumálsins eins og orðaforða, framburð og merkingu.

[Hugbúnaðarhirslur fyrir málföng](https://gitlab.com/icelandic-lt/resources)

[Risamálheildin 1](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/41)

[Risamálheildin 2](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/33)

[Mörkuð íslensk málheild, Gullstaðall](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/39)

[Mörkuð íslensk málheild, Gullstaðall, þjálfunar og prófunarsett](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/40)

[BÍN](https://bin.arnastofnun.is/)

[Íslenskt orðanet](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/69)

[Samhliða málheild ParIce](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/16)

[Þjálfunar- og prófunargögn ParIce](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/24)

[Hljóðgögn frá RÚV sjónvarpi](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/93)


### Málrýni
Málrýni aðstoðar við að leiðrétta texta og skrifa rétt og jafnvel með viðeigandi málsniði. Málrýni hefur einnig brýnu hlutverki að gegna við þróun annars máltæknihugbúnaðar þar sem villur í texta geta haft áhrif á frekari sjálfvirka vinnslu hans. Markmið máltækniáætlunar er að þróa almenna málrýni sem ræður við að finna og leiðrétta algengustu villurnar sem finnast í íslenskum textum. Að til verði þekking á eðli ritvillna hjá mismunandi hópum og að aðferðir verði þróaðar til þess að laga kerfið að mismunandi þörfum, m.a. með tilliti til þjálfunar og kennslu.

[Hugbúnaðarhirslur fyrir málrýni](https://gitlab.com/icelandic-lt/checker)

### Stoðtól
Þó mikið sé um sértækar lausnir í máltækni er ákveðinn grunnhugbúnaður sem nýtist á öllum sviðum hennar. Þetta eru yfirleitt falin tól sem greina grunneiningar í textum, allt frá því að greina hvað teljast orð og hvað ekki, upp í að greina flókið málfræðilegt og merkingarlegt samhengi. Öll þessi tól, sem ekki eru tilbúnar hugbúnaðarlausnir í sjálfu sér, en nauðsynlegur partur máltæknihugbúnaðr og fyrir gagnavinnslu, kallast stoðtól. Dæmi um stoðtól sem unnið er að innan máltækniáætlunar eru textatilreiðari, málfræðilegur markari og þáttarar.

[Hugbúnaðarhirslur fyrir stoðtól](https://gitlab.com/icelandic-lt/nlp)

### Talgerving
Talgerving breytir rituðum texta í talað mál. Tvö meginsvið talgervingarhugbúnaðar eru upplestur og (radd)samskipti. Talgervlar eru notaðir til þess að lesa texta, til dæmis af vefsíðum eða jafnvel heilu bækurnar. Fólk sem af einhverjum ástæðum getur ekki lesið sjálft eða á í erfiðleikum með það treystir á talgervlatækni í daglegu lífi. Samskiptakerfi, þar sem talgreinir nemur það sem notandi segir, þurfa á talgervlum að halda til þess að hægt sé að svara með rödd. Innan máltækniáætlunar er lögð áhersla á að þróa nýjar talgervilsraddir fyrir íslensku, m.a. til þess að notendur hafi val um að hlusta á rödd sem þeim þykir þægileg áheyrnar.

[Hugbúnaðarhirslur fyrir talgervingu](https://gitlab.com/icelandic-lt/tts)

### Talgreining 
Talgreining snýst um það að breyta töluðu máli í ritmál. Hún er forsenda þess að við getum átt samskipti við tölvur og tæki með þeim hætti sem flestum er eðlilegast: með því að tala. Markmið máltækniáætlunar er að til verði almennur íslenskur talgreinir aðgengilegur til notkunar í gegnum vefþjónustu. Allar aðferðir og gögn verði einnig aðgengileg sem grunnur fyrir þróun sérhæfðra talgreina. 

[Hugbúnaðarhirslur fyrir talgreiningu](https://gitlab.com/icelandic-lt/asr)

### Vélþýðingar
Vélþýðingar eru sjálfvirkar þýðingar milli tungumála. Þær eru nú þegar orðnar gagnlegar fyrir ýmis tungumálapör, bæði til þess að hjálpa fólki við að átta sig á innihaldi texta á tungumáli sem það er ekki læst á og til þess að flýta fyrir vinnu þýðenda við tungumál sem þeir eru sérfræðingar í. Enginn þýðingarhugbúnaður ræður hins vegar enn sem komið er við að skila þýðingum sem eru nálægt fullnægjandi gæðum, alltaf þarf að yfirfara texta og laga ef þýðing á að vera nákvæm. Markmið máltækniáætlunar er að til verði opin þýðingarvél sem þýðir á milli íslensku og ensku. Hún á að gagnast við þýðingar á ákveðnum sviðum svo að þýðendur geti fullunnið texta hraðar.

[Hugbúnaðarhirslur fyrir vélþýðingar](https://gitlab.com/icelandic-lt/mt)

