---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Orðtökutól

Orðtökutólið (e. Lexicon Acquisition Tool) er ætlað til þess að safna saman orðum úr [Risamálheildinni](https://malheildir.arnastofnun.is/) sem vantar í önnur textasöfn, fyrst og fremst [Beygingarlýsingu íslensks nútímamáls (BÍN)](bin.arnastofnun.is/) og [Íslenska nútímamálsorðabók (ISLEX)](https://islenskordabok.arnastofnun.is/) en einnig má setja inn eigin orðalista á txt-sniði. 

Mjög góðar leiðbeiningar eru til staðar á [GitLab síðu tólsins](https://gitlab.com/icelandic-lt/nlp/ordtaka#or%C3%B0t%C3%B6kut%C3%B3llexicon-acquisition-tool).  

### Tenglar:
[GitLab. Ordtaka.](https://gitlab.com/icelandic-lt/nlp/ordtaka) 