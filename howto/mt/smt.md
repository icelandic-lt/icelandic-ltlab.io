---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Moses PBSMT

Moses PBSMT (Moses phrase-based statistical machine translation) er kerfi til þess að þróa og keyra tölfræðilegar vélþýðingar.  

Mjög góðar leiðbeiningar eru til staðar á [GitLab síðu tólsins](https://gitlab.com/icelandic-lt/mt/SMT#moses-pbsmt).  

### Heimildir:
[GitLab. SMT.](https://gitlab.com/icelandic-lt/mt/SMT)