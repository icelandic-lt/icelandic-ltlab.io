---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Tiro talgervill

Talgervill frá Tiro ehf sem er enn í þróun.  

### Heimildir:
[GitLab. tiro-tts.](https://gitlab.com/icelandic-lt/tts/tiro-tts)