---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Íslenskar villumálheildir

## Almenn íslensk villumálheild (IceEC)

Íslenska villumálheildin er safn texta á nútímaíslensku þar sem stafsetningar- og málfarsvillur eru merktar og flokkaðar. Textarnir eru flokkaðir eftir textategund. Þessi útgáfa inniheldur málsgreinar úr nemendaritgerðum, fréttum af vefmiðlum og greinum af Wikipedia.  

Villumálheildirnar eru notaðar til þess að flokka og finna dæmi um algengar villur og aðstoða við forgangsröðun við þróun málrýnihugbúnaðar. 
Öll skjöl í málheildinni eru á XML ([Extensible Markup Language](https://en.wikipedia.org/wiki/XML)) formi.  

## Íslensk L2 villumálheild

Íslenska L2 villumálheildin er safn texta á nútímaíslensku sem hafa verið skrifaðir af annarsmálshöfum íslensku.

## Íslensk lesblinduvillumálheild

Íslenska lesblinduvillumálheildin er safn texta á nútímaíslensku sem hafa verið skrifaðir af lesblindum málhöfum með íslensku sem móðurmál. 

## Íslensk barnavillumálheild

Íslenska barnavillumálheildin er safn texta á nútímaíslensku sem hafa verið skrifaðir af börnum á aldrinum 10-15 ára með íslensku sem móðurmál. 


### Dæmi:
![iceerrorcorpus](/assets/img/iceerrorcorpus-img1.png)  
Í þessu dæmi úr málheildinni má sjá að í upprunalegum (<span style="color:blue">original</span>) texta er eintala þar sem á að vera fleirtala (<span style="color:red">singular4plural</span>), og leiðrétt orð (<span style="color:green">corrected</span>).  

### Tenglar:
[GitLab. IceErrorCorpus.](https://gitlab.com/icelandic-lt/resources/iceErrorCorpus#%C3%ADslensk-villum%C3%A1lheild-iceec)
[GitLab. Íslensk L2 villumálheild.](https://gitlab.com/icelandic-lt/resources/iceErrorCorpusL2)
[GitLab. Sérhæfðar íslenskar villumálheildir](https://gitlab.com/icelandic-lt/resources/iceerrorcorpusspezialized)