---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# WebRICE

WebRICE (Web Reader Icelandic) er opinn veflesari fyrir íslensku.  

### Uppsetning og notkun:

Uppsetning krefst þess að vera með Node.js og npm uppsett á vélinni.  

Hægt er að sækja tólið beint af [GitLab síðunni](https://gitlab.com/icelandic-lt/tts/webrice), eða með `git clone`:

`git clone https://gitlab.com/icelandic-lt/tts/webrice.git`

Eins og kemur skýrt fram á GitLab síðunni er uppsetning einföld:  
`npm install`  
og  
`npm run dev`  

Sem ætti að opna vefsíðuna:  

![webrice-mynd1](/assets/img/webrice-img1.png)  

Hægt er að prófa ýmsislegt, t.d. breyta talgervilsrödd `/src/modules/SpeechManager.ts` í Karl eða Dora:  

![dæmi 1](/assets/img/webrice-example1.png)  

Eða bara sjá hvernig talgervillinn les annan texta (t.d. bara í `index.html`):  

![dæmi 2](/assets/img/webrice-example2.png)  
![dæmi 3](/assets/img/webrice-example3.png)  

Innleiða má veflesarann í aðrar síður, nánari upplýsingar eru á [GitLab síðunni undir "usage"](https://gitlab.com/icelandic-lt/tts/WebRICE#usage)

### Tenglar:
[GitLab. WebRICE.](https://gitlab.com/icelandic-lt/tts/WebRICE)