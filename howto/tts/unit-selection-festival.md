---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Unit Selection Festival

Forskrift fyrir íslenskan einingavalstalgervil fyrir The Festival Speech Synthesis System, aðlagað að íslenskum gögnum.  


### Heimildir:
[GitLab. Unit Selection Festival.](https://gitlab.com/icelandic-lt/tts/unit-selection-festival#unit-selection-recipe-for-icelandic)