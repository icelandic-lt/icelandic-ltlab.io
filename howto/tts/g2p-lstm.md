---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# G2P LSTM
Líkan rittákns-til-fónems, oftast kallað g2p-líkan (e. grapheme to phoneme model) er vörpun rittákna yfir í [fónem (stundum kallað hljóðan)](https://is.wikipedia.org/wiki/Hljóðan).  
Þetta er svo notað af talgervlum.  
Þetta g2p tól notar tauganet sem byggir á LSTM (Long short-term memory) og notar fairseq (Facebook AI Research Sequence-to-Sequence) tólið.  

### Uppsetning:
Hægt er að sækja lstm-g2p tólið beint af [GitLab síðunni](https://gitlab.com/icelandic-lt/tts/g2p-lstm), eða með skipanalínutólinu `git`:

`git clone https://gitlab.com/icelandic-lt/tts/g2p-lstm.git`  

Tólið notar [conda](https://docs.conda.io/projects/conda/en/latest/index.html) umhverfi.  
Búa má til nýtt umhverfi í möppu tólsins:  
`conda env create -f environment.yml`  

Hugsanlega þarf að frumstilla conda með skel (t.d. `bash`):  
`conda init bash`  

Og virkja svo með:  
`conda activate fairseq-lstm`  

Eins er hægt að slökkva á conda umhverfi með:  
`conda deactivate`  

ath. ef þú vilt ekki að conda sé alltaf virkt, t.d. eftir endurræsingu:  
`conda config --set auto_activate_base false`  

### Notkun:

Til að prófa getum við búið til textaskjal með orðinu "hlaupa" (ath. skipta verður öllum orðum í einstaka stafi):  
`echo "h l a u p a" > wordlist.txt`  

Því næst er hægt að prófa að keyra g2p vörpun:  
`cat wordlist.txt | ./transcribe_ice_standard wordlist_g2p_standard.out wordlist_g2p_standard.tsv`  

Svo má t.d. skoða `.tsv` úttakið:  
`cat wordlist_g2p_standard.tsv`

Sem ætti að gefa eftirfarandi hljóðritun:  
![dæmi 1](/assets/img/g2p-lstm-example1.png)  

### Heimildir:
[G2P-LSTM. GitLab](https://gitlab.com/icelandic-lt/tts/g2p-lstm)  