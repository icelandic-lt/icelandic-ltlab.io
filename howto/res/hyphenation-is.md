---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Íslenskur orðskiptingalisti

Orðskiptingalisti inniheldur upplýsingar um hvernig á að skipta orðum milli lína.  
![orð-skipt-ing](/assets/img/skiptir-img1.png)  
Slíkir listar eru notaðir í tólinu [Skipti](/howto/nlp/skiptir) auk annars hugbúnaðar eins og t.d. LibreOffice.   


### Tenglar:
[Eldri útgáfa orðskiptingalista](https://clarin.is/gogn/hyphenation/)
[Hyphenation Is, GitLab](https://gitlab.com/icelandic-lt/resources/hyphenation-is)