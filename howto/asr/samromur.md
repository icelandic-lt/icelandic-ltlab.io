---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Samrómur

Samrómur er lýðvirkjunarumhverfi til söfnunar raddgagna fyrir íslensku sem eru svo notuð til þjálfunar talgreiningarhugbúnaðar.  
Gagnasettið samanstendur af [wav](https://en.wikipedia.org/wiki/WAV) hljóðskrám og samsvarandi textaskrám.  

### Heimildir:
[GitLab. Samromur.](https://gitlab.com/icelandic-lt/asr/samromur)