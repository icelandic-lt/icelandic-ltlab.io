---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Samröðun og bútun

Þetta er kerfi skrifta sem notaðar eru til undirbúnings sjónvarpsgagnanna frá RÚV og útvarpsgagnanna frá CreditInfo fyrir talgreiningu.  
Kerfið byggir á [Kaldi](http://kaldi-asr.org/) og er gert fyrir gagnasettið Ruv-di sem er ekki útgefið, auk þess sem kóðaviðbætur vantar í hirsluna.  

### Heimildir:
[GitLab. Alignment And Segmentation.](https://gitlab.com/icelandic-lt/asr/Alignment-and-segmentation)