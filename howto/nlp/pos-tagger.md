---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Málfræðilegur markari fyrir íslensku (Part-of-Speech (PoS) tagger for Icelandic)

[Málfræðilegur markari fyrir íslensku](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic) markar íslenskan texta með málfræðilegum upplýsingum eins og t.d. orðflokki, falli og persónu, samkvæmt [markaskrá (e. tagset)](http://www.malfong.is/files/ot_tagset_files_is.pdf):  


![mörkun](/assets/img/pos-img1.png)  


### Uppsetning og notkun:

Til að nota tólið með forþjálfuðu líkani setjum við það upp með  
`pip install git+https://github.com/cadia-lvl/POS.git@v3.0.0`  

Til að prófa getum við t.d. sótt [`example.txt` skrána sem er í hirslu tólsins](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic/-/raw/master/example.txt?inline=false)
(Gott að hafa hana í sér möppu og opna skel þar).  

Svo getum við keyrt tólið:  
`pos tag example.txt tagged.txt`
[sjá nánar á hirslu tólsins](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic#running-the-models).  

Þá sjáum við hvert orð markað:  
![dæmi 1](/assets/img/pos-example1.png)  


Tólið má einnig nota sem [Python einingu (e. module)](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic#python-module).  

##### Docker:
Hægt er að setja tólið upp í [Docker umhverfi (sjá leiðbeiningar fyrir uppsetningu Docker á hinum ýmsu stýrikerfum)](https://www.docker.com/get-started).  

Til að prófa getum við t.d. sótt [`example.txt` skrána sem er í hirslu tólsins](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic/-/raw/master/example.txt?inline=false)
(Gott að hafa hana í sér möppu og opna skel þar).  

Tólið krefst þess að inntaksskrá sé í UTF-8 kóðun, með einn tóka í hverri línu og setningaskil sýnd með auðri línu.  
Skoða má skrána `example.txt` til að sjá rétt uppsetta textaskrá.  

Docker-mynd tólsins þarf um 6-7 GB af geymsluplássi og mælt er með minnst 4 GB af vinnsluminni.
Til að sækja tólið og jafnframt keyra það í framhaldinu notum við eftirfarandi skipun:  
`docker run -v $PWD:/data haukurp/pos:1.0.0 /data/example.txt /data/tagged.txt`  

![dæmi 2](/assets/img/pos-example2.png)  
  
Eftir það má skoða skrána `tagged.txt` sem er með málfræðilegum mörkum fyrir hvert orð, skipt með dálkstaf (e. Tab Character).  

### Tenglar:

[POS tagger for Icelandic. GitLab](https://gitlab.com/icelandic-lt/nlp/pos-tagger-for-icelandic#docker)