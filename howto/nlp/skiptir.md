---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Skiptir

[Skiptir](https://gitlab.com/icelandic-lt/nlp/skiptir) er orðskiptingatól fyrir íslensku sem er notað innan verkefna SÍM (Samstarf um íslenska máltækni). Tólið er notað til að skipta orðum eftir nýjustu orðskiptingamynstrum fyrir íslensku (sjá á [CLARIN.is](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/86) og [GitLab](https://gitlab.com/icelandic-lt/resources/hyphenation-is)).  
[Vefviðmót](http://malvinnsla.arnastofnun.is/ordskipting) fyrir tólið er einnig í boði.

## Hvað er orðskipting?

Orðskipting skilgreinir hvar er leyfilegt að skipta orðum í hluta, t.d. milli lína:  

![orð-skipt-ing](/assets/img/skiptir-img1.png)  
Þessar skilgreiningar eru notaðar í ýmsum tólum, m.a. fyrir leiðréttingu stafsetningar og málfars.


### Notkun:

`./skiptir.py [--mode MODE] [--hyphen HYPHEN]`  

Tólið les frá inntaki og prentar orðskiptan texta í úttaki.

MODE (háttur) er sjálfgefinn sem 'pattern', sem notar [Pyphen](https://github.com/Kozea/Pyphen) með nýjustu orðskiptingamynstrum fyrir íslensku.  
Aðrir hættir eru ekki studdir eins og er.  

HYPHEN (bandstrik) er sérvalið skiptingatákn, t.d. · eða -.  
Sjálfgefið skiptingatákn er skiptivísir ([e. soft hyphen](https://en.wikipedia.org/wiki/Hyphen#Soft_and_hard_hyphens)) (U+00AD).

### Dæmi:

Einfalt prufudæmi er að keyra tólið í skipanalínu:  
`./skiptir.py`  
og skrifa inn texta og ýta á enter, og ljúka svo með lausnarrununni (e. escape sequence) `CTRL + d`: 

![dæmi 1](/assets/img/skiptir-example1.png)  

Tólið er þó hannað til að taka við textaskrám og prentar þá skipt úttak textaskrár í skipanagluggann. Skráin input.txt fylgir með tólinu og er notuð hér sem dæmi:  

![dæmi 2](/assets/img/skiptir-example2.png) 

Einnig er hægt að beina úttaki í skrá (og skoða skrána svo, t.d. með tólinu `cat`):  

![dæmi 3](/assets/img/skiptir-example3.png) 

Til að nota annað skiptingatákn, t.d. stjörnu (*), er `--hyphen` stiki notaður:  

![dæmi 4](/assets/img/skiptir-example4.png) 

### Tenglar:
[Skiptir. GitLab](https://gitlab.com/icelandic-lt/nlp/skiptir)