---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# Vélþýðing

[Velthyding.is](https://velthyding.is) er framendi fyrir tauganetsþýðingarvél Miðeindar, útfærður í Javascript og React.  

![velthyding-mynd1](/assets/img/velthyding-img1.png)  
Á vefsíðunni er hægt að þýða texta og skjöl á milli íslensku og ensku.  
Bæði vefurinn og vélin eru í örri þróun.

### Uppsetning og notkun
Uppsetning krefst þess að vera með Node.js og npm uppsett á vélinni.  

Hægt er að sækja tólið beint af [GitLab síðunni](https://gitlab.com/icelandic-lt/mt/velthyding), eða með skipanalínutólinu `git`:

`git clone https://gitlab.com/icelandic-lt/mt/velthyding.git`


### Heimildir:
[GitLab. Velthyding.](https://gitlab.com/icelandic-lt/mt/velthyding)