---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# NER

Nafnakennsl eða NER (Named Entity Recognition) er ferli til að finna og flokka nafneiningar í texta, t.d. persónunöfn, staðarnöfn/örnefni, fyrirtæki, dagsetningar o.fl.  

Nafnaþekkjarinn fyrir íslensku byggist á [ELECTRA](https://github.com/google-research/electra) og er fínstilltur fyrir nafnakennsl með [MIM-GOLD-NER](https://repository.clarin.is/repository/xmlui/handle/20.500.12537/42) málheildinni.


### Tenglar:
[GitLab. NER.](https://gitlab.com/icelandic-lt/nlp/ner)