---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# GreynirCorrect

[GreynirCorrect](https://gitlab.com/icelandic-lt/checker/GreynirCorrect) er málrýnir fyrir íslenskan texta. Málrýnir finnur stafsetningar- og málfarsvillur og aðstoðar þannig við skrif, hægt er að prófa málrýninn á vefsíðunni [yfirlestur.is](https://yfirlestur.is/). 

## Hvað er málrýnir?

Málrýnir les yfir texta og finnur stafsetningar- og málfarsvillur[^1] og leiðréttir þær og/eða gefur ábendingar um það sem betur má fara.  

![greynircorrect-mynd1](/assets/img/greynircorrect-img1.png)
![greynircorrect-mynd2](/assets/img/greynircorrect-img2.png)   

Málrýnir nýtist einnig í máltæknihugbúnaði þar sem textagreining getur batnað ef villur eru leiðréttar áður en greining fer fram.


### Notkun og dæmi:

Góðar leiðbeiningar um uppsetningu GreynirCorrect má finna á [GreynirCorrect síðunni](https://gitlab.com/icelandic-lt/checker/GreynirCorrect). Þegar pakkinn hefur verið settur upp er hægt að prófa málrýninn með `correct` á skipanalínu:

`correct [-h] [--csv | --json | [--spaced] [infile] [outfile]`  
  

Einfalt prufudæmi er að nota `echo` og pípu (`|`) til þess að senda texta til leiðréttingar:

![dæmi 1](/assets/img/greynircorrect-example1.png)  

Einnig er hægt að stilla form úttaksskrár á csv eða json (eins og fyrir [Tilreiðarann](/howto/tokenizer)) t.d.:

![dæmi 2](/assets/img/greynircorrect-example2.png)  

  

#### Tenglar:  
[GreynirCorrect](https://gitlab.com/icelandic-lt/checker/GreynirCorrect)

[Yfirlestur](https://yfirlestur.is/)  
