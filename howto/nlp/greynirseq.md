---
title: SÍM
subtitle: Samstarf um íslenska máltækni
layout: page
show_sidebar: false
---

# GreynirSeq

GreinirSeq er tólasett til að vinna með íslensku, sem er í þróun og er enn á frumstigi.[^1]

### Heimildir:
[^1]: [GitLab. GreynirSeq.](https://gitlab.com/icelandic-lt/nlp/GreynirSeq)